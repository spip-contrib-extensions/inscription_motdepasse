<?php

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'inscriptionmotdepasse_nom' => 'Mot de passe dès l’inscription',
	'inscriptionmotdepasse_slogan' => 'Ajoute un champ pour définir le mot de passe dès l’inscription.',
	'inscriptionmotdepasse_description' => 'Ce plugin permet aux visiteurs qui peuvent s’inscrire de définir leur mot de passe dès la phase d’inscription, plutôt que recevoir un mot de passe aléatoire qu’ils voudront changer ensuite.',
);
