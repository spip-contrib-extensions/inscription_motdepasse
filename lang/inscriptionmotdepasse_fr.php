<?php

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'erreur_email_non_confirme' => 'Merci de confirmer votre adresse email avant d\'utiliser votre compte'
);
