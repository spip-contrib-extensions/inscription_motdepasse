**Mot de passe dès l'inscription**

Ajoute un champ pour définir le mot de passe dès l’inscription. 
Plus simple pour les utilisateurs/trices, et plus sécurisé.

Documentation sur

https://contrib.spip.net/4802